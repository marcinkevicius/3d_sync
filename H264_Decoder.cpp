#include "H264_Decoder.h"

#define ROXLU_IMPLEMENTATION

H264_Decoder::H264_Decoder()
  :codec(NULL)
  ,codec_context(NULL)
  ,parser(NULL)
  ,fp(NULL)
  ,frame(0)
  ,cb_frame(0)
  ,frame_timeout(0)
  ,frame_delay(0)
  ,code_word(0)
  ,frame_time_stamp(0)
{
  avcodec_register_all();
}
 
H264_Decoder::~H264_Decoder() {
 
  if(parser) {
    av_parser_close(parser);
    parser = NULL;
  }
 
  if(codec_context) {
    avcodec_close(codec_context);
    av_free(codec_context);
    codec_context = NULL;
  }
 
  if(picture) {
    av_free(picture);
    picture = NULL;
  }
 
  if(fp) {
    fclose(fp);
    fp = NULL;
  }
 
  cb_frame = NULL;
  frame = 0;
  frame_timeout = 0;
}
 
bool H264_Decoder::load(int port_number)//std::string filepath)//, float fps) 
{
 
  //TODO : codec is not accessible for 20s after program exits. WTF? :/
  codec = avcodec_find_decoder(AV_CODEC_ID_H264);
  if(!codec) {
    printf("Error: cannot find the h264 codec: \n");
    return false;
  }
 
  codec_context = avcodec_alloc_context3(codec);
 
  if(codec->capabilities & CODEC_CAP_TRUNCATED) {
    codec_context->flags |= CODEC_FLAG_TRUNCATED;
  }
 
  if(avcodec_open2(codec_context, codec, NULL) < 0) {
    printf("Error: could not open codec.\n");
    return false;
  }
  
  /* First call to socket() function */     //mine
  sockfd = socket(AF_INET, SOCK_STREAM, 0); //mine
  
  if (sockfd < 0) {
      perror("ERROR opening socket");
      return false;
  }
  
  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = port_number;//5001;
   
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
   
  /* Now bind the host address using bind() call.*/
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      return false;
  }
      
   /* Now start listening for the clients, here process will
      * go in sleep mode and will wait for the incoming connection
   */
   
  listen(sockfd,5);
  clilen = sizeof(cli_addr);
   
   /* Accept actual connection from the client */
  newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
	
  if (newsockfd < 0) {
      perror("ERROR on accept");
      return false;
  }
   
   /* If connection is established then start communicating */
  bzero(socket_buffer, H264_INBUF_SIZE);
    
  picture = av_frame_alloc();
  parser = av_parser_init(AV_CODEC_ID_H264);
 
  if(!parser) {
    printf("Erorr: cannot create H264 parser.\n");
    return false;
  }

  // kickoff reading...
  no_data_count=0; // set no data error to 0
  printf("looks ok \n");
  readBuffer();
 
  return true;
}
 
bool H264_Decoder::readFrame() {
 
  bool needs_more = false;
 
  while(!update(needs_more)) { 
    if(needs_more) {
      readBuffer();
    }
    
    //std::cout << "no_data_count -> " << no_data_count;
    if (no_data_count>100){ //if data if failing to arrive, then close the connection
          return false;
      }
  }
 
  return true;
}
 
void H264_Decoder::decodeFrame(uint8_t* data, int size) {
 
  AVPacket pkt;
  int got_picture = 0;
  int len = 0;
 
  av_init_packet(&pkt);
 
  pkt.data = data;
  pkt.size = size;
 
  len = avcodec_decode_video2(codec_context, picture, &got_picture, &pkt); // TODO, offload to GPU!
  if(len < 0) {
    printf("Error while decoding a frame.\n");
  }
 
  if(got_picture == 0) {
    return;
  } else 
      cb_frame=true;
 
  ++frame;

}
 
int H264_Decoder::readBuffer() { //bybiene kai NAL atsirando ties 512 riba, laikuit esant butina permastyt
  
  int bytes_read = read(newsockfd, inbuf, H264_INBUF_SIZE );
  int bytes_read_all = bytes_read;
  
  if(bytes_read) {
    // search for nal unit
    int i=0;
    while (i < bytes_read ) {
        code_word = (code_word | inbuf[i]) << 8;
        if (code_word == 0x00000100){
            //printf("got nal unit");
            code_word = 0;
            //printf("time stamp || %u || ", ((inbuf[i+2] << 8) | (inbuf[i+1])));//inbuf[(i+1)], inbuf[(i+2)]);
            bytes_read=bytes_read-2;
/*            if (bytes_read == i){ // check this
                printf("WE ARE ATH THE END");
            }*/
            // overwrite timestamp data
            for (int y=(i+1); y<=bytes_read; y=y+2){
                inbuf[y]=inbuf[y+2];
                inbuf[y+1]=inbuf[y+3];
            }
        }
        i++;
    }
    //  copy bytes to buffer
    std::copy(inbuf, inbuf + bytes_read, std::back_inserter(buffer));
  }
  
  if ((bytes_read_all) < H264_INBUF_SIZE ){
    boost::this_thread::sleep( boost::posix_time::milliseconds(30) );
    printf( "bites -> %10d",bytes_read);
  }
  
  if (bytes_read){
      no_data_count=0;
  } else no_data_count++;
  
  return bytes_read;
}
 
bool H264_Decoder::update(bool& needsMoreBytes) {
 
  needsMoreBytes = false;
 
  if (!newsockfd){ //mine
    printf("Cannot update .. file not opened...\n");
    return false;
  }
 
  if(buffer.size() == 0) {
    needsMoreBytes = true;
    return false;
  }
 
  uint8_t* data = NULL;
  int size = 0;
  int len = av_parser_parse2(parser, codec_context, &data, &size, 
                             &buffer[0], buffer.size(), 0, 0, AV_NOPTS_VALUE);
 
  if(size == 0 && len >= 0) {
    needsMoreBytes = true;
    return false;
  }
 
  if(len) {
    decodeFrame(&buffer[0], size);
    buffer.erase(buffer.begin(), buffer.begin() + len);
    return true;
  }
 
  return false;
}