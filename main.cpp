/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: kid
 *
 * Created on August 6, 2016, 12:17 AM
 */

#include <cstdlib>
#include "H264_Decoder.h"
#include <cstdlib>
#include "opencv2/opencv.hpp"
#include <boost/thread.hpp>

//sudo insmod /lib/modules/3.19.0-42-generic/updates/dkms/nvidia-uvm.ko

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/avutil.h>
#include <libavutil/pixfmt.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <opencv2/core/mat.hpp>
}

using namespace std;
using namespace cv;

/*
 * 
 */

Mat left_result; // left result
bool left_ready=0;
int left_time_stamp;
AVFrame dst_left;
struct SwsContext *convert_ctx_l;
boost::mutex left_cam_mutex;
Mat left_planes[3];

Mat right_result; // left result
bool right_ready=0;
int right_time_stamp;
AVFrame dst_right;
struct SwsContext *convert_ctx_r;
boost::mutex right_cam_mutex;
Mat right_planes[3];
Mat original_3D(1080, 1920, CV_8UC3);

int from_to[] = { 0,0, 1,1, 2,2 };

boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
boost::posix_time::ptime end = boost::posix_time::second_clock::local_time();

void leftCam() {
    Mat m;
    H264_Decoder * ddec = new H264_Decoder();
    ddec->load(5001);
    while(ddec->readFrame()){
        if (ddec->cb_frame) {
            ddec->cb_frame=false;
            int w = ddec->picture->width;
            int h = ddec->picture->height;
            m = cv::Mat(h, w, CV_8UC3);
            dst_left.data[0] = (uint8_t *)m.data;
            av_image_fill_arrays(dst_left.data, dst_left.linesize, dst_left.data[0], AV_PIX_FMT_BGR24, w, h, 1);
            
            enum AVPixelFormat src_pixfmt = (enum AVPixelFormat)ddec->picture->format;
            
            enum AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;

            convert_ctx_l = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt, SWS_FAST_BILINEAR, NULL, NULL, NULL);
            
            if(convert_ctx_l == NULL) {
                fprintf(stderr, "Cannot initialize the conversion context!\n");
                exit(1);
            }
            sws_scale(convert_ctx_l, ddec->picture->data, ddec->picture->linesize, 0, h, dst_left.data, dst_left.linesize);
            
            left_cam_mutex.lock();
            m.copyTo(left_result);
            left_ready=1;
            left_time_stamp=ddec->frame_time_stamp;
            left_cam_mutex.unlock();
        }
    } 
}
void rightCam() {
    Mat m;
    H264_Decoder * ddec = new H264_Decoder();
    ddec->load(5002);
    while(ddec->readFrame()){
        printf("b");
        if (ddec->cb_frame) {
            ddec->cb_frame=false;
            int w = ddec->picture->width;
            int h = ddec->picture->height;
            m = cv::Mat(h, w, CV_8UC3);
            dst_right.data[0] = (uint8_t *)m.data;
            av_image_fill_arrays(dst_right.data, dst_right.linesize, dst_right.data[0], AV_PIX_FMT_BGR24, w, h, 1);
            
            enum AVPixelFormat src_pixfmt = (enum AVPixelFormat)ddec->picture->format;
            
            enum AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;

            convert_ctx_r = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt, SWS_FAST_BILINEAR, NULL, NULL, NULL);
            
            if(convert_ctx_r == NULL) {
                fprintf(stderr, "Cannot initialize the conversion context!\n");
                exit(1);
            }
            sws_scale(convert_ctx_r, ddec->picture->data, ddec->picture->linesize, 0, h, dst_right.data, dst_right.linesize);
            
            right_cam_mutex.lock(); // TOD pakeist callback'u procesui mergeCams
            m.copyTo(right_result);
            right_ready=1;
            right_time_stamp=ddec->frame_time_stamp;
            right_cam_mutex.unlock();
        }
    } 
}

void mergeCams() {
    Mat left_frame_array [20]; // last frames of video
    int left_frame_time_stamps [20]; // time stamps of those frames
    int left_now=-1; // current position on array
    
    Mat right_frame_array [20]; // look above
    int right_frame_time_stamps [20];
    int right_now=-1;
    
    int got_frame=0;

    //int i = 0;
    while (true) {
        if (left_ready)
        {
            //i++;
            //imshow("leftVideo", left_result);
            left_now=(left_now+1) % 20;
            left_ready=0;
            left_result.copyTo(left_frame_array[left_now]);
            left_frame_time_stamps[left_now]=left_time_stamp;
            
            //printf(" showing %i \n" , i );
            //waitKey(1); // remove when not debuging
            //boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            
            got_frame=got_frame+1;
        }
        
        if (right_ready)
        {
            //imshow("rightVideo", right_result);
            right_now=(right_now+1) % 20;
            right_ready=0;
            right_result.copyTo(right_frame_array[right_now]);
            right_frame_time_stamps[right_now]=right_time_stamp;
            
            //printf(" showing %i \n" , i );
            //waitKey(1); // remove when not debuging
            //boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            
            got_frame=got_frame+2;
        }
        
        if (!(got_frame>2))
        {boost::this_thread::sleep(boost::posix_time::milliseconds(10));}
        
        else {
            if (!(left_frame_array[left_now].empty() || right_frame_array[right_now].empty()) && left_now > -1 && right_now > -1 ){
                split(left_frame_array[left_now],left_planes);
                split(right_frame_array[right_now],right_planes);
                Mat in[] = {left_planes[0], left_planes[1], right_planes[2]};
                
                mixChannels( in , 3, &original_3D, 1, from_to, 3 );
                imshow("3dVideo", original_3D);
                waitKey(1); // remove when not debuging
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
                got_frame=0;
            }
        }
    }
}

int main(int argc, char** argv) {

    /*
    ddec = new H264_Decoder();
    ddec->load("/home/kid/Desktop/buffer");
    while(ddec->readFrame()){
        if (ddec->cb_frame) {
            ddec->cb_frame=false;
            int w = ddec->picture->width;
            int h = ddec->picture->height;
            m = cv::Mat(h, w, CV_8UC3);
            dst.data[0] = (uint8_t *)m.data;
            av_image_fill_arrays(dst.data, dst.linesize, dst.data[0], AV_PIX_FMT_BGR24, w, h, 1);
            
            enum AVPixelFormat src_pixfmt = (enum AVPixelFormat)ddec->picture->format;
            
            enum AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;

            convert_ctx = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt, SWS_FAST_BILINEAR, NULL, NULL, NULL);
            
            if(convert_ctx == NULL) {
                fprintf(stderr, "Cannot initialize the conversion context!\n");
                exit(1);
            }
            sws_scale(convert_ctx, ddec->picture->data, ddec->picture->linesize, 0, h, dst.data, dst.linesize);
            
            anagl_result=m;
            
            imshow("MyVideo", anagl_result);
            //outputVideo << anagl_result;
            waitKey(1);

        }
        waitKey(1);
/*        t2 = boost::posix_time::second_clock::local_time();
        boost::posix_time::time_duration diff = t2 - t1;
        std::cout << "tim diff -> " << diff.total_milliseconds() << std::endl;
        t1 = boost::posix_time::second_clock::local_time();
    } */
    boost::thread leftC(leftCam);
    boost::thread rightC(rightCam);
    boost::thread mergeC(mergeCams);
    
    leftC.join();
    rightC.join();
    return 0;
}

